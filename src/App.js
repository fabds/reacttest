import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Container, Col, Row , Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

import Card from './Components/Card/Card.js';

class App extends Component {

  constructor(props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar() {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  testClickHandler(title) {
    alert(title)
  }

  render() {
    return (
      <div className="App">
        <Container>
          <Row>
            <Col>
            <Navbar color="faded" light>
              <NavbarBrand href="/" className="mr-auto">FabTest</NavbarBrand>
              <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
              <Collapse isOpen={!this.state.collapsed} navbar>
                <Nav navbar>
                  <NavItem>
                    <NavLink href="#">Link 1</NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink href="#">Link 2</NavLink>
                  </NavItem>
                </Nav>
              </Collapse>
            </Navbar>
            </Col>
          </Row>
          <Row>
            <Col>
              <Navbar toggle={this.toggleNavbar} />
            </Col>
          </Row>
        </Container>
        <Container>
          <Row>
            <Col md="4">
              <Card 
                title="Card 1" 
                description="Lorem ipsum dolor sit amet 1" 
                subtitle="Sottotitolo 1" 
                button="Button 1" 
                click={this.testClickHandler.bind(this,'test 1')}
              />
            </Col>
            <Col md="4">
              <Card 
                title="Card 2" 
                description="Lorem ipsum dolor sit amet 2" 
                subtitle="Sottotitolo 2" 
                button="Button 2" 
                click={this.testClickHandler.bind(this,'test 1')}
              />
            </Col>
            <Col md="4">
              <Card 
                title="Card 3" 
                description="Lorem ipsum dolor sit amet 3" 
                subtitle="Sottotitolo 3" 
                button="Button 3" 
                click={this.testClickHandler.bind(this,'test 1')}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <br></br>
              <Button color="danger">Danger!</Button>
            </Col>
          </Row>
        </Container>
        
      </div>
    );
  }
}

export default App;
